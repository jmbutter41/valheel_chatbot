Mix.install([
    :jason
])

Code.require_file("boards.ex")
Code.require_file("boards/board.ex")

defmodule ExSudoku do
    def load_board(filename) do
        filename
        |> File.read!()
        |> Jason.decode!()
    end

    def test() do
        Boards.test() |> IO.inspect()
        #|> Boards.valid?() |> IO.inspect()
        |> Boards.solve() 
    end
end

ExSudoku.test()