defmodule Boards.Board do
    defstruct [
        :rows
    ]

    def new(row_list) do
        rows =
            row_list
            |> Enum.with_index(fn r, i ->
                Enum.with_index(r, fn v, j ->
                    {{i, j}, v}
                end)
            end)
            |> List.flatten()
            |> Enum.into(%{})

        %Boards.Board{
            rows: rows
        }
    end
end
