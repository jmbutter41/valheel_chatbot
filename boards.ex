Code.require_file("boards/board.ex")

defmodule Boards do

    @all_digits MapSet.new([1,2,3,4,5,6,7,8,9])

    alias Boards.Board

    def test() do
        # b = 1..9 |> Enum.map(& 1..9 |> Stream.cycle() |> Stream.drop(&1*3) |> Stream.take(9) |> Enum.into([]))
        b = load_board!("boards/1.json")
        IO.inspect(length(b.rows |> Map.keys()), label: "board size")
        b
    end

    def load_board!(filename) do
        filename
        |> File.read!()
        |> Jason.decode!()
        |> Board.new()
    end

    def put_number(board, n, i, j) do
        new_rows =
            board.rows
            |> Map.put({i, j}, n)
        
        %Boards.Board{rows: new_rows}
    end

    def solve(board, solved \\ false) do
        do_solve(board, solved, true)
    end

    def do_solve(board, true = _solved, _numbers_to_try) do
        {board, true} |> IO.inspect(label: "solved board")
    end

    def do_solve(board, false, false) do
        IO.inspect("board is unsolvable")
        {board, false}
    end

    def do_solve(board, false = solved, numbers_to_try) do
        # cases:
        # 1. board is solved
        # 2. ran out of numbers to try.  dead end
        # 3. everything still looks valid and there are still numbers to tru

        # for each nil space: 
        # look at the row, column, and square to determine which numbers could fit
        # try each one and then try to solve the rest of the board
        {{i, j}, _v} =
            board.rows
            |> Enum.filter(fn {_k, v} -> v == nil end) 
            |> Enum.sort()
            |> IO.inspect(label: "empty spaces")
            |> List.first()

            #|> Enum.reduce_while({board, false}, fn {{i, j}, _v}, {acc_board, _solved} ->
            #    {solved_board, solved} =
           #         acc_board
        {solved_board, solved} =
            board
            |> possible_numbers(i, j) |> IO.inspect()
            |> Enum.reduce_while({board, false}, fn n, {inner_acc_board, _solved} ->
                new_board = inner_acc_board |> put_number(n, i, j) #|> IO.inspect(label: "new_board")
                IO.inspect("put number #{n} at #{i}, #{j}")
                        
                case new_board |> solved?() do
                    true -> {:halt, {new_board, true}}
                    false -> {:cont, do_solve(new_board, solved, true)}
                end
            end)
        
        # ran out of numbers to try, so this is a dead end.  try the next one
        do_solve(solved_board, solved, false)
    end
    
    def solved?(board) do
        solved_cells =
            board.rows
            |> Enum.filter(fn {_k, v} -> v end)
            |> length() 
            
        solved_cells == 81 and valid?(board)
    end

    def possible_numbers(board, i, j) do
        IO.inspect({i, j}, label: "getting possible numbers for")
        {{i1, j1}, {i2, j2}} = get_row_coords(i, j)
        row_numbers =
            board.rows
            |> Enum.filter(fn {{i, j}, v} ->
                i >= i1 and i <= i2 and j >= j1 and j <= j2 and v != nil
            end)
            |> Enum.map(fn {_k, v} -> v end)

        {{i1, j1}, {i2, j2}} = get_col_coords(i, j)
        col_numbers =
            board.rows
            |> Enum.filter(fn {{i, j}, v} ->
                i >= i1 and i <= i2 and j >= j1 and j <= j2 and v != nil
            end)
            |> Enum.map(fn {_k, v} -> v end)

        {{i1, j1}, {i2, j2}} = get_square_coords(i, j)
        square_numbers = 
            board.rows
            |> Enum.filter(fn {{i, j}, v} ->
                i >= i1 and i <= i2 and j >= j1 and j <= j2 and v != nil
            end)
            |> Enum.map(fn {_k, v} -> v end)

        [row_numbers, col_numbers, square_numbers]
        |> Enum.concat()
        |> MapSet.new()
        |> then(&MapSet.difference(@all_digits, &1))
    end

    def get_row_coords(i, j) do
        get_coord_set(i,j, row_coords())
    end

    def get_col_coords(i, j) do
        get_coord_set(i, j, col_coords())
    end

    def get_square_coords(i, j) do
        get_coord_set(i, j, square_coords())
    end

    def get_coord_set(i, j, sets) do
        #IO.inspect(i)
        #IO.inspect(j)
        # IO.inspect(sets)
        Enum.find(sets, fn {{i1, j1}, {i2, j2}} ->
            i >= i1 and i <= i2 and j >= j1 and j <= j2
        end)
        #|> IO.inspect()
    end

    def valid?(board) do
        # set_valid?(board.rows) and set_valid?(board.columns) and set_valid?(board.squares)
        coord_set_valid?(board, row_coords()) and
            coord_set_valid?(board, col_coords()) and
            coord_set_valid?(board, square_coords())
    end

    def row_coords do
        0..8
        |> Enum.map(fn i -> 
            {{i, 0}, {i, 8}}
        end)
    end

    def col_coords do
        0..8
        |> Enum.map(fn i ->
            {{0, i}, {8, i}}
        end)
    end

    def square_coords do
        [
            {{0,0}, {2,2}},
            {{3,0}, {5,2}},
            {{6,0}, {8,2}},
            {{0,3}, {2,5}},
            {{3,3}, {5,5}},
            {{6,3}, {8,5}},
            {{0,6}, {2,8}},
            {{3,6}, {5,8}},
            {{6,6}, {8,8}}
        ]
    end

    def coord_set_valid?(board, set) do
        set
        |> Enum.all?(fn {start_coord, end_coord} -> coords_valid?(board, start_coord, end_coord) end)
    end

    def coords_valid?(board, {i1, j1}, {i2, j2}) do
        board.rows
        |> Enum.filter(fn {{i, j}, v} ->
            i >= i1 and i <= i2 and j >= j1 and j <= j2 and v != nil
        end)
        |> Enum.frequencies_by(fn {_k,v} -> v end)
        |> Enum.all?(fn {_k, v} -> v == 1 end)
    end
end