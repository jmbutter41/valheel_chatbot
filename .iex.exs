Application.put_env(:elixir, :ansi_enabled, true)

# IEx shell customization for color
# - Added as a comment: https://thinkingelixir.com/course/code-flow/module-1/pipe-operator/
# - Original source: https://samuelmullen.com/articles/customizing_elixirs_iex/
# - Can add to a specific project or can put in the User home folder as a global config.
# Updates from:
# - https://github.com/am-kantox/finitomata/blob/48e1b41b21f878e9720e6562ca34f1ce7238d810/examples/ecto_intergation/.iex.exs
timestamp = fn ->
  {_date, {hour, minute, _second}} = :calendar.local_time
  [hour, minute]
  |> Enum.map(&(String.pad_leading(Integer.to_string(&1), 2, "0")))
  |> Enum.join(":")
end

IEx.configure(
  colors: [
    syntax_colors: [
      number: :light_yellow,
      atom: :light_cyan,
      string: :light_black,
      boolean: :red,
      nil: [:magenta, :bright],
    ],
    ls_directory: :cyan,
    ls_device: :yellow,
    doc_code: :green,
    doc_inline_code: :magenta,
    doc_headings: [:cyan, :underline],
    doc_title: [:cyan, :bright, :underline],
  ],
  default_prompt:
    "#{IO.ANSI.green}%prefix#{IO.ANSI.reset} " <>
    "[#{IO.ANSI.magenta}#{timestamp.()}#{IO.ANSI.reset} " <>
    ":: #{IO.ANSI.cyan}%counter#{IO.ANSI.reset}] >",
  alive_prompt:
    "#{IO.ANSI.green}%prefix#{IO.ANSI.reset} " <>
    "(#{IO.ANSI.yellow}%node#{IO.ANSI.reset}) " <>
    "[#{IO.ANSI.magenta}#{timestamp.()}#{IO.ANSI.reset} " <>
    ":: #{IO.ANSI.cyan}%counter#{IO.ANSI.reset}] >",
  history_size: 50,
  inspect: [
    pretty: true,
    limit: :infinity,
    width: 80,
    # Fix for map key sorting: `data = %{a: 1, b: 2, c: 3, d: 4}`
    custom_options: [sort_maps: true]
  ],
  width: 80
)

alias Morpheus.AI
alias Morpheus.Scraper
alias Morpheus.TextSearch
alias Morpheus.Chunks
alias Morpheus.Chunks.Chunk
alias Morpheus.Chunks.VssChunk
alias Morpheus.Interactions
alias Morpheus.Interactions.Interaction
alias Morpheus.Pages
alias Morpheus.Pages.Page
alias Morpheus.Embeddings
alias Morpheus.Chats
alias Morpheus.DiscordConsumer

import Ecto.Query, warn: false