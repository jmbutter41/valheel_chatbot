defmodule Morpheus.Repo.Migrations.CreateInteractions do
  use Ecto.Migration

  def change do
    create table(:interactions) do
      add :interaction_id, :integer
      add :guild_id, :integer
      add :channel_id, :integer
      add :token, :string
      add :crawl_id, :string

      timestamps(type: :utc_datetime)
    end

    create index(:interactions, [:interaction_id])
    create index(:interactions, [:crawl_id])
  end
end
