defmodule Morpheus.Repo.Migrations.CreateChunks do
  use Ecto.Migration

  def change do
    create table(:chunks) do
      add :title, :string
      add :content, :string
      add :embedding, :vector
      add :crawl_id, :string
      add :page_id, references(:pages, on_delete: :delete_all)

      timestamps(type: :utc_datetime)
    end

    create index(:chunks, [:crawl_id])
    create index(:chunks, [:page_id])
  end
end
