defmodule Morpheus.Repo.Migrations.AddFts5ChunksTable do
  use Ecto.Migration

    def up do
    execute(
      """
      CREATE VIRTUAL TABLE fts5_chunks USING fts5(
        title,
        content
      );
      """
    )
  end

  def down do
    execute(
      """
      DROP TABLE fts5_chunks;
      """
    )
  end
end
