defmodule Morpheus.Repo.Migrations.AddApplicationIdToInteractions do
  use Ecto.Migration

  def change do
    alter table(:interactions) do
      add :application_id, :integer
    end
  end
end
