defmodule Morpheus.Repo.Migrations.CreatePages do
  use Ecto.Migration

  def change do
    create table(:pages) do
      add :url, :string
      add :title, :string
      add :content, :string
      add :crawl_id, :string

      timestamps(type: :utc_datetime)
    end

    create index(:pages, [:url])
    create index(:pages, [:crawl_id])
  end
end
