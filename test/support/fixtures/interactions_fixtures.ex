defmodule Morpheus.InteractionsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Morpheus.Interactions` context.
  """

  @doc """
  Generate a interaction.
  """
  def interaction_fixture(attrs \\ %{}) do
    {:ok, interaction} =
      attrs
      |> Enum.into(%{
        crawl_id: "some crawl_id",
        interaction_id: 42,
        token: "some token"
      })
      |> Morpheus.Interactions.create_interaction()

    interaction
  end
end
