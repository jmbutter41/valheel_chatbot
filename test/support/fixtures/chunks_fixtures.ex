defmodule Morpheus.ChunksFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Morpheus.Chunks` context.
  """

  @doc """
  Generate a chunk.
  """
  def chunk_fixture(attrs \\ %{}) do
    {:ok, chunk} =
      attrs
      |> Enum.into(%{
        content: "some content",
        title: "some title"
      })
      |> Morpheus.Chunks.create_chunk()

    chunk
  end
end
