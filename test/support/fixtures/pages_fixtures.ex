defmodule Morpheus.PagesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Morpheus.Pages` context.
  """

  @doc """
  Generate a page.
  """
  def page_fixture(attrs \\ %{}) do
    {:ok, page} =
      attrs
      |> Enum.into(%{
        title: "some title",
        url: "some url"
      })
      |> Morpheus.Pages.create_page()

    page
  end
end
