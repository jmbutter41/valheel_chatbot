defmodule Morpheus.InteractionsTest do
  use Morpheus.DataCase

  alias Morpheus.Interactions

  describe "interactions" do
    alias Morpheus.Interactions.Interaction

    import Morpheus.InteractionsFixtures

    @invalid_attrs %{crawl_id: nil, interaction_id: nil, token: nil}

    test "list_interactions/0 returns all interactions" do
      interaction = interaction_fixture()
      assert Interactions.list_interactions() == [interaction]
    end

    test "get_interaction!/1 returns the interaction with given id" do
      interaction = interaction_fixture()
      assert Interactions.get_interaction!(interaction.id) == interaction
    end

    test "create_interaction/1 with valid data creates a interaction" do
      valid_attrs = %{crawl_id: "some crawl_id", interaction_id: 42, token: "some token"}

      assert {:ok, %Interaction{} = interaction} = Interactions.create_interaction(valid_attrs)
      assert interaction.crawl_id == "some crawl_id"
      assert interaction.interaction_id == 42
      assert interaction.token == "some token"
    end

    test "create_interaction/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Interactions.create_interaction(@invalid_attrs)
    end

    test "update_interaction/2 with valid data updates the interaction" do
      interaction = interaction_fixture()
      update_attrs = %{crawl_id: "some updated crawl_id", interaction_id: 43, token: "some updated token"}

      assert {:ok, %Interaction{} = interaction} = Interactions.update_interaction(interaction, update_attrs)
      assert interaction.crawl_id == "some updated crawl_id"
      assert interaction.interaction_id == 43
      assert interaction.token == "some updated token"
    end

    test "update_interaction/2 with invalid data returns error changeset" do
      interaction = interaction_fixture()
      assert {:error, %Ecto.Changeset{}} = Interactions.update_interaction(interaction, @invalid_attrs)
      assert interaction == Interactions.get_interaction!(interaction.id)
    end

    test "delete_interaction/1 deletes the interaction" do
      interaction = interaction_fixture()
      assert {:ok, %Interaction{}} = Interactions.delete_interaction(interaction)
      assert_raise Ecto.NoResultsError, fn -> Interactions.get_interaction!(interaction.id) end
    end

    test "change_interaction/1 returns a interaction changeset" do
      interaction = interaction_fixture()
      assert %Ecto.Changeset{} = Interactions.change_interaction(interaction)
    end
  end
end
