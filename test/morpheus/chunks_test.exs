defmodule Morpheus.ChunksTest do
  use Morpheus.DataCase

  alias Morpheus.Chunks

  describe "chunks" do
    alias Morpheus.Chunks.Chunk

    import Morpheus.ChunksFixtures

    @invalid_attrs %{title: nil, content: nil}

    test "list_chunks/0 returns all chunks" do
      chunk = chunk_fixture()
      assert Chunks.list_chunks() == [chunk]
    end

    test "get_chunk!/1 returns the chunk with given id" do
      chunk = chunk_fixture()
      assert Chunks.get_chunk!(chunk.id) == chunk
    end

    test "create_chunk/1 with valid data creates a chunk" do
      valid_attrs = %{title: "some title", content: "some content"}

      assert {:ok, %Chunk{} = chunk} = Chunks.create_chunk(valid_attrs)
      assert chunk.title == "some title"
      assert chunk.content == "some content"
    end

    test "create_chunk/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chunks.create_chunk(@invalid_attrs)
    end

    test "update_chunk/2 with valid data updates the chunk" do
      chunk = chunk_fixture()
      update_attrs = %{title: "some updated title", content: "some updated content"}

      assert {:ok, %Chunk{} = chunk} = Chunks.update_chunk(chunk, update_attrs)
      assert chunk.title == "some updated title"
      assert chunk.content == "some updated content"
    end

    test "update_chunk/2 with invalid data returns error changeset" do
      chunk = chunk_fixture()
      assert {:error, %Ecto.Changeset{}} = Chunks.update_chunk(chunk, @invalid_attrs)
      assert chunk == Chunks.get_chunk!(chunk.id)
    end

    test "delete_chunk/1 deletes the chunk" do
      chunk = chunk_fixture()
      assert {:ok, %Chunk{}} = Chunks.delete_chunk(chunk)
      assert_raise Ecto.NoResultsError, fn -> Chunks.get_chunk!(chunk.id) end
    end

    test "change_chunk/1 returns a chunk changeset" do
      chunk = chunk_fixture()
      assert %Ecto.Changeset{} = Chunks.change_chunk(chunk)
    end
  end
end
