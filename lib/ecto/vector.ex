defmodule Ecto.Vector do
  use Ecto.Type

  def type, do: :vector

  def cast(data) when is_list(data) do
    {:ok, data}
  end

  def cast(%Nx.Tensor{} = data) do
    {:ok, Nx.to_flat_list(data)}
  end

  def cast(_), do: :error

  def load(data) when is_binary(data) do
    Jason.decode(data)
  end

  def dump(data) when is_list(data) do
    Jason.encode(data)
  end
 end
