defmodule Morpheus.Interactions.Interaction do
  use Ecto.Schema
  import Ecto.Changeset

  @required_attrs [
    :application_id,
    :interaction_id,
    :token,
    :guild_id,
    :channel_id
  ]

  @optional_attrs [
    :crawl_id
  ]

  @all_attrs @required_attrs ++ @optional_attrs

  schema "interactions" do
    field :application_id, :integer
    field :crawl_id, :string
    field :interaction_id, :integer
    field :token, :string
    field :guild_id, :integer
    field :channel_id, :integer

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(interaction, attrs) do
    interaction
    |> cast(attrs, @all_attrs)
    |> validate_required(@required_attrs)
  end
end
