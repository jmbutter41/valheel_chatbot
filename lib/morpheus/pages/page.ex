defmodule Morpheus.Pages.Page do
  use Ecto.Schema
  import Ecto.Changeset

  alias Morpheus.Chunks.Chunk

  @optional_attrs [
    :content,
    :crawl_id,
    :title
  ]

  @required_attrs [:url]

  @all_attrs @optional_attrs ++ @required_attrs

  schema "pages" do
    field :title, :string
    field :url, :string
    field :content, :string
    field :crawl_id, :string

    has_many :chunks, Chunk

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(page, attrs) do
    page
    |> cast(attrs, @all_attrs)
    |> validate_required(@required_attrs)
  end
end
