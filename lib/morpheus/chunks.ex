defmodule Morpheus.Chunks do
  @moduledoc """
  The Chunks context.
  """

  import Ecto.Query, warn: false

  alias Ecto.Multi
  alias Morpheus.Repo

  alias Morpheus.Chunks.Chunk
  alias Morpheus.Chunks.Fts5Chunk
  alias Morpheus.Embeddings
  alias Morpheus.SearchStore

  @doc """
  Returns the list of chunks.

  ## Examples

      iex> list_chunks()
      [%Chunk{}, ...]

  """
  def list_chunks do
    Repo.all(Chunk)
  end

  @doc """
  Gets a single chunk.

  Raises `Ecto.NoResultsError` if the Chunk does not exist.

  ## Examples

      iex> get_chunk!(123)
      %Chunk{}

      iex> get_chunk!(456)
      ** (Ecto.NoResultsError)

  """
  def get_chunk!(id), do: Repo.get!(Chunk, id)

  def get_nearest_chunks(term, options \\ []) do
    f = get_nearest_chunks_fts5(term, options)
    v = get_nearest_chunks_vector(term, options)

    f ++ v
  end

  def get_nearest_chunks_vector(term, opts) do
    case Embeddings.create_query_embedding(term) do
      {:ok, vector} -> 
        SearchStore.lookup(vector, opts)
        |> Repo.preload(:page)
 
      _ ->
        []
    end
  end

  def get_nearest_chunks_fts5(term, options) do
    limit = Keyword.get(options, :limit, 3)
    max_rank = Keyword.get(options, :max_rank, -4.5)
    IO.inspect(options, label: "opts in get_nearest_chunks_fts5")
    lexical_term = ~s("#{term}")
    from(f in Fts5Chunk,
      select: [:id, :rank, :title],
      where: fragment("fts5_chunks MATCH ? and rank MATCH 'bm25(10.0)'", ^lexical_term),
      order_by: [asc: :rank],
      limit: ^limit,
      preload: [chunk: :page]
    )
    |> Repo.all()
    |> Enum.filter(& &1.rank > max_rank)
  end

  @doc """
  Creates a chunk.

  ## Examples

      iex> create_chunk(%{field: value})
      {:ok, %Chunk{}}

      iex> create_chunk(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_chunk(attrs \\ %{}) do
    # %Chunk{}
    # |> Chunk.changeset(attrs)
    # |> Repo.insert()

    Multi.new()
    |> multi_create_chunk(attrs)
    |> Repo.transaction()
  end

  def rebuild_search_indexes() do
    Repo.checkout(fn ->
      # rebuild_vss_index()
      rebuild_fts5_index()
    end)

    SearchStore.rebuild_index()
    #%{pid: repo_pid} = Ecto.Repo.Registry.lookup(Repo)
    #DBConnection.disconnect_all(repo_pid, 1000)
  end

  def rebuild_fts5_index() do
      ddl_queries = [
        "DROP TABLE IF EXISTS fts5_chunks;",
        # DROP TABLE vss_chunks_data;
        # DROP TABLE vss_chunks_index;
        "CREATE VIRTUAL TABLE fts5_chunks USING fts5(title, content);"
      ]
      q = "INSERT INTO fts5_chunks(rowid, title, content) SELECT id, title, content FROM chunks;"

      Enum.each(ddl_queries, fn ddl_q -> 
        Repo.query([ddl_q])
      end)

      # Ecto.Adapters.SQL.query!(Repo, q, [])
      Repo.query(q)
  end

  def multi_create_chunk(multi, attrs \\ %{}) do
    uid = Ecto.UUID.generate()
    chunk_name = "chunk:#{uid}"
    #vss_chunk_name = "vss_chunk:#{uid}"
    #fts5_chunk_name = "fts5_chunk:#{uid}"

    multi
    |> Multi.insert(chunk_name, fn multi_data -> 
      multi_data = multi_data || %{}
      Chunk.changeset(%Chunk{}, Map.put_new_lazy(attrs, :page_id, fn -> multi_data |> Map.get(:page) |> Map.get(:id) end))
    end)
  end

  @doc """
  Deletes a chunk.

  ## Examples

      iex> delete_chunk(chunk)
      {:ok, %Chunk{}}

      iex> delete_chunk(chunk)
      {:error, %Ecto.Changeset{}}

  """
  def delete_chunk(%Chunk{} = chunk) do
    Multi.new()
    |> multi_delete_chunk(chunk)
    |> Repo.transaction()
  end

  def multi_delete_chunk(multi, %Chunk{} = chunk) do
    uid = Ecto.UUID.generate()
    chunk_name = "delete_chunk:#{uid}"
    
    multi
    |> Multi.delete(chunk_name, chunk)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking chunk changes.

  ## Examples

      iex> change_chunk(chunk)
      %Ecto.Changeset{data: %Chunk{}}

  """
  def change_chunk(%Chunk{} = chunk, attrs \\ %{}) do
    Chunk.changeset(chunk, attrs)
  end
end
