defmodule Morpheus.Chunks.Chunk do
  use Ecto.Schema
  import Ecto.Changeset

  alias Ecto.Vector
  alias Morpheus.Pages.Page

  @optional_attrs []

  @required_attrs [
    :embedding,
    :title,
    :content,
    :page_id,
    :crawl_id
  ]

  @all_attrs @optional_attrs ++ @required_attrs

  schema "chunks" do
    field :title, :string
    field :content, :string
    field :embedding, Vector
    field :crawl_id, :string

    belongs_to :page, Page

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(chunk, attrs) do
    chunk
    |> cast(attrs, @all_attrs)
    |> validate_required(@required_attrs)
  end
end
