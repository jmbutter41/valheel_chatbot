defmodule Morpheus.Chunks.Fts5Chunk do
  use Ecto.Schema
  import Ecto.Changeset

  alias Morpheus.Chunks.Chunk

  @optional_attrs []

  @required_attrs [:id, :title, :content]

  @all_attrs @optional_attrs ++ @required_attrs

  @primary_key {:id, :id, autogenerate: true, source: :rowid}
  schema "fts5_chunks" do
    field :title, :string
    field :content, :string
    field :rank, :float, virtual: true

    has_one :chunk, Chunk, foreign_key: :id
  end

  @doc false
  def changeset(chunk, attrs) do
    chunk
    |> cast(attrs, @all_attrs)
    |> validate_required(@required_attrs)
  end
end
