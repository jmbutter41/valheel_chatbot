defmodule Morpheus.Chats do

    alias Nostrum.Api
    alias Morpheus.AI

    def respond_to_message(message, channel_id) do
        with true <- should_respond?(message),
             {:ok, _chain, ai_response} <- message |> String.replace(~r/hey morpheus/i, "") |> AI.ask_question() do
            send_message(ai_response.content, channel_id)
        end
    end

    defp send_message(message, channel_id) do
        Api.create_message(channel_id, message)
    end

    defp should_respond?(message) do
        String.downcase(message) =~ "hey morpheus"
    end
end
