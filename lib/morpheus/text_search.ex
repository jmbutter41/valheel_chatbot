defmodule Morpheus.TextSearch do
    alias Morpheus.Chunks
    alias Morpheus.Embeddings
    alias Morpheus.Pages

    def get_relevant_info_function() do
        LangChain.Function.new!(%{
            name: "get_revant_info",
            description: "Finds the most relevant information to a given subject",
            parameters_schema: %{
                type: "object",
                properties: %{
                    search_term: %{
                        type: "string",
                        description: "The subject to search for"
                    }
                },
                required: [:search_term],
            },
            function: &get_relevant_info/2
        })
    end

    def get_relevant_info(%{"search_term" => search_term}, _context) do
        with {:ok, query_vector} <- Embeddings.create_query_embedding(search_term) do
            query_vector
            |> Chunks.get_nearest_chunks()
            |> Pages.get_pages_for_chunks()
            |> Enum.map(&format_page/1)
            |> then(fn list -> %{result: list} end)
            |> Jason.encode!()
        end
    end

    def get_relevant_info_function_ft() do
        LangChain.Function.new!(%{
            name: "get_revant_info",
            description: "Finds the most relevant information to a given search subject",
            parameters_schema: %{
                type: "object",
                properties: %{
                    search_term: %{
                        type: "string",
                        description: "The subject to search for"
                    }
                },
                required: [:search_term],
            },
            function: &get_relevant_info_ft/2
        })
    end

    def get_relevant_info_ft(%{"search_term" => search_term}, _context) do
            search_term
            |> get_matches(max_distance: 0.75, max_rank: -5)
            |> then(fn list -> %{result: list} end)
            |> Jason.encode!()
    end

    def get_matches(term, options \\ []) do
        options = add_default_options(options)
        IO.inspect(options, label: "get_matches options")
        term
        |> Chunks.get_nearest_chunks(options)
        |> Pages.get_pages_for_chunks()
        |> Enum.map(&format_page/1)
    end

    defp format_page(page) do
        %{
            page_title: page.title,
            page_url: page.url,
            page_content: page.content
        }
    end

    defp add_default_options(options) do
        options
        |> Keyword.put_new(:max_distance, Application.get_env(:morpheus, Morpheus.TextSearch)[:max_distance])
        |> Keyword.put_new(:max_rank, Application.get_env(:morpheus, Morpheus.TextSearch)[:max_rank])
        |> Keyword.put_new(:limit, Application.get_env(:morpheus, Morpheus.TextSearch)[:limit])
    end
end
