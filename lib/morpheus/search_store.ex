defmodule Morpheus.SearchStore do
  use Agent

  @space :cosine
  @dimensions 768
  @max_elements 10000
  @m 100

  alias Morpheus.Chunks

  @doc """
  Starts a new search store.
  """
  def start_link(_opts) do
    Agent.start_link(
        fn -> do_rebuild_index() end, 
        name: __MODULE__
    )
  end

  @doc """
  Rebuilds the index
  """
  def rebuild_index() do
    Agent.update(
        __MODULE__, 
        fn _ -> do_rebuild_index() end,
        30_000
    )
  end

  def lookup(embedding, opts \\ []) do
    Agent.get(
        __MODULE__,
        fn
            nil -> 
                []
            index -> 
                do_lookup(index, embedding, opts)
        end,
        15_000
    )
  end

  defp fill_index(index) do
    get_embeddings()
    |> Enum.unzip()
    |> then(fn 
      {[], []} ->
        index
      {ids, embeddings} ->
        HNSWLib.Index.add_items(index, Nx.tensor(embeddings, type: :f32), ids: ids)
        index
    end)
  end

  defp get_embeddings() do
    Chunks.list_chunks()
    |> Enum.map(fn chunk ->
        {chunk.id, chunk.embedding}
    end)
  end

  defp do_lookup(index, embedding, opts) do
    k = Keyword.get(opts, :limit)
    max_distance = Keyword.get(opts, :max_distance)
    IO.inspect(opts, label: "opts in do_lookup")

    case HNSWLib.Index.knn_query(index, Nx.tensor(embedding, type: :f32), k: k) do
        {:ok, ids, distances} ->
            ids
            |> Nx.to_flat_list()
            |> Enum.zip(Nx.to_flat_list(distances))
            |> Enum.map(fn {id, distance} -> {Chunks.get_chunk!(id), distance} end)
            |> Enum.filter(fn {_chunk, distance} -> distance < max_distance end)
            |> Enum.map(fn {chunk, _distance} -> chunk end)

        _ ->
            []
    end
  end

  defp do_rebuild_index() do
    case HNSWLib.Index.new(@space, @dimensions, @max_elements, m: @m) do
        {:ok, index} -> 
            fill_index(index)
        _ -> 
            nil
    end
  end
end
