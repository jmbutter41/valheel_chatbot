defmodule Morpheus.DiscordConsumer do

    use Nostrum.Consumer

    alias Nostrum.Struct.Interaction
    alias Morpheus.AI
    alias Morpheus.Chats
    alias Morpheus.Interactions
    alias Morpheus.Scraper
    alias Morpheus.TextSearch

    @command_option_types %{
        sub_command: 1,
        sub_command_group: 2,
        string: 3
    }

    @commands [
        # %{
        #     name: "ask",
        #     description: "Ask Morpheus a question",
        #     options: [
        #         %{
        #             type: @command_option_types.string,
        #             description: "the question to ask",
        #             name: "question",
        #             required: true
        #         }
        #     ],
        #     type: 1
        # },
        %{
            name: "find",
            description: "Find something on the wiki",
            options: [
                %{
                    type: @command_option_types.string,
                    description: "the question to ask",
                    name: "question",
                    required: true
                }
            ],
            type: 1
        },
        %{
            name: "morpheus",
            description: "Commands for interacting with Morpheus",
            options: [
                %{
                    type: @command_option_types.sub_command,
                    description: "scrape the wiki for information",
                    name: "scrape",
                    options: []
                }
            ],
            type: 1,
            default_member_permissions: "0"
        }
    ]

    def handle_event({:MESSAGE_CREATE, msg, _ws_state}) do
        Chats.respond_to_message(msg.content, msg.channel_id)
    end

    def handle_event({:INTERACTION_CREATE,
        interaction = %Interaction{
            data: %{
                name: "ask",
                options: [
                    %{
                        name: "question",
                        value: question
                    }
                ]
            }
        },
        _ws_state
    }) do
        respond_to_interaction_question(interaction, question)
    end

    def handle_event({:INTERACTION_CREATE,
        interaction = %Interaction{
            data: %{
                name: "find",
                options: [
                    %{
                        name: "question",
                        value: term
                    }
                ]
            }
        },
        _ws_state
    }) do
        respond_to_interaction_find(interaction, term)
    end

    def handle_event({:INTERACTION_CREATE,
        interaction = %Interaction{
            data: %{
                name: "morpheus",
                options: [
                    %{
                        name: "ask",
                        options: [
                            %{
                                name: "question",
                                value: question
                            }
                        ]
                    }
                ]
            }
        }, _ws_state}) do
        respond_to_interaction_question(interaction, question)
    end

    def handle_event({:INTERACTION_CREATE,
        interaction = %Interaction{
            data: %{
                name: "morpheus",
                options: [
                    %{
                        name: "scrape",
                        options: []
                    }
                ]
            }
        }, _ws_state}) do
        crawl_id = Ecto.UUID.generate()

        with {:ok, _saved_interaction} <- Interactions.create_interaction(%{
            interaction_id: interaction.id,
            guild_id: interaction.guild_id,
            channel_id: interaction.channel_id,
            token: interaction.token,
            application_id: interaction.application_id,
            crawl_id: crawl_id
        }) do
            Scraper.start_scraper([crawl_id: crawl_id])
        end

        Nostrum.Api.create_interaction_response(interaction, %{type: 5})
    end

    def handle_event({:READY, msg, _wg_state}) do
        Enum.each(msg.guilds, fn guild ->
            create_commands(guild.id)
        end)
    end

    def handle_event(e) do
        IO.inspect(e)
    end

    defp respond_to_interaction_question(interaction, question) do
        Nostrum.Api.create_interaction_response(interaction, %{type: 5})
        Task.start(fn ->
            with {:ok, _chain, %{content: response_text}} <- AI.ask_question(question) do
                Nostrum.Api.edit_interaction_response(interaction, %{content: response_text})
            else
                _ ->
                    Nostrum.Api.edit_interaction_response(interaction, %{content: "Error!"})
            end
        end)
    end

    defp respond_to_interaction_find(interaction, term) do
        Nostrum.Api.create_interaction_response(interaction, %{type: 5})
        Task.start(fn ->
            case TextSearch.get_matches(term) do
                [] ->
                    response_text = "I couldn't find anything."
                    Nostrum.Api.edit_interaction_response(interaction, %{content: response_text})

                l when is_list(l) ->
                    response_text =
                        l
                        |> Enum.map(& &1.page_url)
                        |> Enum.join("\n")
                    Nostrum.Api.edit_interaction_response(interaction, %{content: response_text})
            end
        end)
    end

    def create_commands(guild_id) do
        Enum.each(@commands, fn cmd ->
            Nostrum.Api.create_guild_application_command(guild_id, cmd)
        end)
    end
end
