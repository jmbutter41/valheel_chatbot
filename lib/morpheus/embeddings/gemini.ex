defmodule Morpheus.Embeddings.Gemini do

    require Logger

    @model "embedding-001"

    @endpoint "https://generativelanguage.googleapis.com/v1beta/models/#{@model}:embedContent"

    def create_embedding(title, content) do
        req = Req.new(
            url: build_url(),
            json: %{
                content: format_content(content),
                taskType: "RETRIEVAL_DOCUMENT",
                title: title
            }
        )

        with {:ok, %{status: 200, body: %{"embedding" => %{"values" => values}}}} <- Req.post(req) do
            {:ok, Nx.tensor(values)}
        else
          _resp ->
            Logger.warning("Failed to create embedding for: #{title}: #{content}")
            {:error, :failed}
        end
    end

    def create_query_embedding(query) do
        req = Req.new(
            url: build_url(),
            json: %{
                content: format_content(query),
                taskType: "RETRIEVAL_QUERY"
            }
        )

        with {:ok, %{status: 200, body: %{"embedding" => %{"values" => values}}}} <- Req.post(req) do
            {:ok, Nx.tensor(values)}
        end
    end

    defp format_content(content) do
        %{
            parts: [
                %{
                    text: content
                }
            ],
            role: :user
        }
    end

    defp build_url() do
        "#{@endpoint}?key=#{api_key()}"
    end

    defp api_key, do: Application.get_env(:langchain, :google_ai_key)
end
