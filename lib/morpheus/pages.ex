defmodule Morpheus.Pages do
  @moduledoc """
  The Pages context.
  """

  import Ecto.Query, warn: false

  alias Ecto.Multi

  alias Morpheus.Chunks
  alias Morpheus.Chunks.Chunk
  alias Morpheus.Repo
  alias Morpheus.Pages.Page

  @doc """
  Returns the list of pages.

  ## Examples

      iex> list_pages()
      [%Page{}, ...]

  """
  def list_pages do
    Repo.all(Page)
  end

  @doc """
  Gets a single page.

  Raises `Ecto.NoResultsError` if the Page does not exist.

  ## Examples

      iex> get_page!(123)
      %Page{}

      iex> get_page!(456)
      ** (Ecto.NoResultsError)

  """
  def get_page!(id), do: Repo.get!(Page, id)

  def get_page_by(clauses, opts \\ []) do
    res =
      Repo.get_by(Page, clauses, opts)
    
    if opts[:preload] do
      Repo.preload(res, opts[:preload])
    else
      res
    end
  end

  def list_pages_without_crawl_id(crawl_id, opts \\ []) do
    res = 
      from(p in Page,
      where: p.crawl_id != ^crawl_id or is_nil(p.crawl_id))
      |> Repo.all()

    if opts[:preload] do
      Repo.preload(res, opts[:preload])
    else
      res
    end
  end

  @doc """
  Creates a page.

  ## Examples

      iex> create_page(%{field: value})
      {:ok, %Page{}}

      iex> create_page(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_page(attrs \\ %{}) do
    %Page{}
    |> Page.changeset(attrs)
    |> Repo.insert()
  end

  def multi_create_page(multi, attrs \\ %{}) do
    multi
    |> Multi.insert(:page, Page.changeset(%Page{}, attrs))
  end

  def get_pages_for_chunks(chunks) do
    chunks
    |> Enum.group_by(& &1.__struct__)
    |> Enum.flat_map(
      fn 
        {Chunk, chunks} -> chunks |> Repo.preload(:page) |> Enum.map(& &1.page)
        {_, chunks} -> chunks |> Repo.preload(chunk: :page) |> Enum.map(& &1.chunk.page)
      end)
    |> Enum.uniq()
  end

  @doc """
  Updates a page.

  ## Examples

      iex> update_page(page, %{field: new_value})
      {:ok, %Page{}}

      iex> update_page(page, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_page(%Page{} = page, attrs) do
    page
    |> Page.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a page.

  ## Examples

      iex> delete_page(page)
      {:ok, %Page{}}

      iex> delete_page(page)
      {:error, %Ecto.Changeset{}}

  """
  def delete_page(%Page{} = page) do
    Repo.delete(page)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking page changes.

  ## Examples

      iex> change_page(page)
      %Ecto.Changeset{data: %Page{}}

  """
  def change_page(%Page{} = page, attrs \\ %{}) do
    Page.changeset(page, attrs)
  end

  def multi_delete_page_for_url(multi, url) do
    %{url: url}
    |> get_page_by()
    |> then(fn page -> multi_delete_page(multi, page) end)
  end

  def multi_delete_page(multi, page) do
    page = Repo.preload(page, :chunks)
    uid = Ecto.UUID.generate()
    delete_page_name = "delete_page:#{uid}"

    if page do
      page.chunks
      |> Enum.reduce(multi, fn chunk, m2 ->
        Chunks.multi_delete_chunk(m2, chunk)
      end)
      |> Multi.delete(delete_page_name, page)
    else
      multi
    end
  end

  def multi_delete_pages_for_other_crawl_ids(multi, crawl_id) do
    pages = list_pages_without_crawl_id(crawl_id)

    Enum.reduce(pages, multi, fn page, multi ->
      multi_delete_page(multi, page)
    end)
  end

  def delete_pages_for_other_crawl_ids(crawl_id) do
    Multi.new()
    |> multi_delete_pages_for_other_crawl_ids(crawl_id)
    |> Repo.transaction()
  end

  def upsert_page_and_chunks(page, chunks) do
    Multi.new()
    #|> multi_delete_page_for_url(page.url)
    |> multi_create_page(page)
    |> then(fn m2 -> 
      Enum.reduce(chunks, m2, fn chunk, m3 -> 
        Chunks.multi_create_chunk(m3, chunk)
      end)
    end)
    |> Repo.transaction()
  end
end
