defmodule Morpheus.Scraper do
  @behaviour Crawly.Spider

  alias Morpheus.Embeddings
  alias Morpheus.Chunks
  alias Morpheus.Interactions
  alias Morpheus.Pages

  def start_scraper(opts \\ []) do
    Crawly.Engine.start_spider(
      __MODULE__,
      opts
    )
  end

  def on_spider_closed_callback(_spider_name, crawl_id, reason) do
    if reason == :spider_finished do
      # delete all pages and chunks that arent for this crawl id
      Pages.delete_pages_for_other_crawl_ids(crawl_id)
      Chunks.rebuild_search_indexes()
    end

    response_text =
      case reason do
        :spider_finished -> "Scrape finished!"
        r -> "Scrape error! #{inspect(r)}"
      end

    case Interactions.get_interaction_by(crawl_id: crawl_id) do
      nil ->
        nil
      interaction ->
        # respond to interaction
        Nostrum.Api.create_followup_message(interaction.application_id, interaction.token, %{content: response_text})
    end
  end

  @split_by_headers ["h2", "h3"]
  @impl Crawly.Spider
  def base_url(), do: "https://valheel.fandom.com/"

  @impl Crawly.Spider
  def init(_ \\ []) do
    [
      start_urls: [
        "https://valheel.fandom.com/wiki/Special:AllPages"
      ]
    ]
  end

  @impl Crawly.Spider
  def override_settings() do
    []
  end

  @impl Crawly.Spider
  def parse_item(response) do
    with {:ok, html} <- Floki.parse_document(response.body) do
      {:ok, crawl_id} = Crawly.Engine.get_crawl_id(__MODULE__)
      _interaction = Interactions.get_interaction_by(crawl_id: crawl_id)
      
      page_title =
        html
        |> Floki.find(".mw-page-title-main")
        |> Floki.text(sep: " ")

      content =
        html
        |> filter_content()

      page = %{
        url: response.request_url, 
        title: page_title, 
        content: Floki.raw_html(content),
        crawl_id: crawl_id
      }

      items =
        content
        |> chunk_content(page_title, crawl_id)
        |> create_embeddings()

      Pages.upsert_page_and_chunks(page, items)

      requests =
        html
        |> Floki.find("ul.mw-allpages-chunk li a")
        |> Enum.flat_map(&Floki.attribute(&1, "href"))
        |> Enum.uniq()
        |> Enum.map(&build_absolute_url/1)
        |> Enum.map(&Crawly.Utils.request_from_url/1)

        %Crawly.ParsedItem{items: items, requests: requests}
    else
      _ -> %Crawly.ParsedItem{items: [], requests: []}
    end
  rescue
    e ->
      {:ok, crawl_id} = Crawly.Engine.get_crawl_id(__MODULE__)
      interaction = Interactions.get_interaction_by(crawl_id: crawl_id)
      Nostrum.Api.create_followup_message(interaction.application_id, interaction.token, %{content: "Error scraping page: #{response.request_url}\nError: #{inspect(e)}"})
      %Crawly.ParsedItem{items: [], requests: []}
  end

  def chunk_content(content, page_title, crawl_id) do
    grouped_content =
      content
      |> split_content_by_headers()
      |> normalize_headers(page_title)
      |> split_by([".", "!", "?"])

    items =
      grouped_content
      |> Enum.map(fn {title, content} ->
        %{
          title: title,
          content: content,
          crawl_id: crawl_id
        }
      end)

    items
  end

  def create_embeddings(items) do
    items
    |> Enum.map(fn item ->
      {:ok, embedding} = Embeddings.create_embedding(item.title, item.content)
      Map.put(item, :embedding, embedding)
    end)
  end

  def save_items(items) do
    items
    |> Enum.map(fn item ->
      Chunks.create_chunk(item)
    end)
  end

  def filter_content(content) do
    content
    |> Floki.find(".mw-parser-output")
    |> Floki.filter_out(".toc")
    |> Floki.filter_out(".incontent-player")
    |> Floki.filter_out(:comment)
    |> List.first()
    |> Floki.children()
    || []
  end

  def pull_test_page() do
    Crawly.fetch("https://valheel.fandom.com/wiki/Isel%27s_Revenge")
    |> Map.get(:body)
    |> filter_content()
  end

  def split_content_by_headers(content) do
    content
    |> Enum.chunk_while(
      [],
      fn
        {s, _,_} = el, acc when s in @split_by_headers ->
          {:cont, Enum.reverse(acc), [el]}
        el, acc ->
          {:cont, [el | acc]}
      end,
      fn acc -> {:cont, Enum.reverse(acc), []} end
    )
    |> Enum.reject(&Enum.empty?/1)
  end

  def normalize_headers(split_content, page_title) do
    split_content
    |> Enum.map(fn doc ->
    #   heading =
    #     doc
    #     |> Floki.find(".mw-headline")
    #     |> List.first()
    #     |> Floki.text(sep: " ")

      {"#{page_title}", doc}
    end)
  end

  def split_by(docs, chars) do
    orig =
      docs
      |> Enum.map(fn {title, content} ->
        {title, content |> Floki.text(sep: " ")}
      end)
    
    shortened =
      docs
      |> Enum.flat_map(fn {title, content} -> 
        content 
        |> Floki.text(sep: " ")
        |> String.split(chars, trim: true)
        |> Enum.map(&String.trim/1)
        |> Enum.reject(fn s -> s == "" end)
        |> Enum.map(fn s -> {title, s} end)
      end)
    
    orig ++ shortened
  end

  # End result: [[h1, el1, el2], [h2, el3, el4]]
  def group_content(content, group_by_element \\ "h2") do
    {groups, _} = Enum.reduce(content, {[], []}, fn
      {^group_by_element, _, _} = e, {acc, part_acc} ->
        {[Enum.reverse(part_acc) | acc], [e]}
      e, {acc, part_acc} ->
        {acc, [e | part_acc]}
    end)

    Enum.reverse(groups)
  end

  defp build_absolute_url(url), do: URI.merge(base_url(), url) |> to_string()
end
