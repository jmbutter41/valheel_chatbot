defmodule Morpheus.StorageBucket do
  use Agent

  @doc """
  Starts a new storage bucket.
  """
  def start_link(_opts) do
    Agent.start_link(fn -> {%{}, 0} end, name: __MODULE__)
  end

  @doc """
  Gets a value from the `bucket` by `key`.
  """
  def get(key) do
    Agent.get(__MODULE__, fn {data, _next_id} ->
      Map.get(data, key)
    end)
  end

  def get_all() do
    Agent.get(__MODULE__, fn {data, _next_id} ->
      data
    end)
  end

  @doc """
  Puts the `value` for the given `key` in the `bucket`.
  """
  def upsert(key, value) do
    Agent.get_and_update(__MODULE__, fn {data, next_id} ->
      value = Map.put(value, :id, key)
      new_data = Map.put(data, key, value)
      {value, {new_data, next_id}}
    end)
  end

  def insert(value) do
    Agent.get_and_update(__MODULE__, fn {data, next_id} ->
      value = Map.put(value, :id, next_id)
      new_data = Map.put(data, next_id, value)
      {value, {new_data, next_id + 1}}
    end)
  end
end
