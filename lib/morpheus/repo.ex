defmodule Morpheus.Repo do
  use Ecto.Repo, otp_app: :morpheus, adapter: Ecto.Adapters.SQLite3
end
