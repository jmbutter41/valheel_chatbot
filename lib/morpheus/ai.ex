defmodule Morpheus.AI do

    alias LangChain.Message
    alias LangChain.Chains.LLMChain
    alias LangChain.ChatModels.ChatGoogleAI

    alias Morpheus.TextSearch

    @endpoint "https://generativelanguage.googleapis.com"

    def ask_question(message) do
        ask_question(nil, message)
    end

    def ask_question(nil, message) do
        new_chain()
        |> ask_question(message)
    end

    def ask_question(chain, message) do
        case TextSearch.get_matches(message, max_distance: 0.75, max_rank: -5) do
            [] ->
                {:ok, chain, %{content: "Couldnt find anything"}}
            l when is_list(l) ->
                chain
                |> LLMChain.add_message(Message.new_user!(system_prompt(message, l)))
                |> LLMChain.run(while_needs_response: true)
        end
    end

    def new_chain() do
        %{llm: ChatGoogleAI.new!(%{endpoint: @endpoint, api_key: Application.get_env(:langchain, :google_ai_key)}), verbose: true, verbose_deltas: true}
        |> LLMChain.new!()
        #|> LLMChain.add_functions(TextSearch.get_relevant_info_function_ft())
        #|> LLMChain.add_message(
        #    Message.new_system!(system_prompt())
        #)
    end

    def system_prompt(question, pages) do
        """
        Follow these steps:
        1. For each page in the context, determine if it is relevant to the question based on the page_title and page_content.  If it is not relevant, remove it from the list.
        2. Extract the page_url for each page and return them in a list of raw urls.  It must not be json-encoded.

        Question: #{question}

        Context:
            #{Jason.encode!(pages)}
        """
    end

    def system_prompt() do
    #   """

    #   You are an AI assistant that will be asked questions about the contents of a wiki page.

    #   The wiki contains references to quests called chapters.

    #   Use the following steps to answer the question.  Keep your answer concise.  If you are not sure, answer "I don't know".

    #   1. Extract the subject that needs to be looked up to answer the question
    #   2. Use the provided get_relevant_info function to look up the most relevant pages from the wiki
    #   3. Use the information in the page_content to answer the question.  Don't use information from any other sources.  Keep your answer short.  Do not answer with a question.

    #   Always include the urls of the pages where the information comes from.

    #   The pages returned by the function will be in a json object with the page title, page url, and page content in it.  The page content will be im html format.  For example, the json object might look like this:
    #   {
    #     "page_title": "Cars",
    #     "page_url": "https://cars.com"
    #     "page_content": "<div>Cars</div>"
    #   }]
    #   """
    # """
    #     You are an AI assistant that will be asked questions about the contents of a wiki page.

    #     The wiki contains references to quests called chapters.

    #     Use the following steps to respond to the question.

    #     1. Extract the subject that needs to be looked up to answer the question.  The subject is most likely to be an Item, Armor, Weapon, Gear, Guide, or Chapter.  Questions about Armor or Weapons might be answered best by oages about Gear.  Questions about
    #     2. Use the provided get_relevant_info function to look up the most relevant pages from the wiki
    #     3. Respond only with the urls of the pages most likely to answer the question.  Do not try to answer the question directly.  Do not modify the urls.  If there are multiple pages, list them all.  If there are none, answer "Your question may be too complex for me.  Try asking about a specific Item, Guide, Armor, Weapon, Gear, or Chapter."

    #     Example:
        
    #     Question: 
    #         What is a mirra infused pickaxe?
    #     Response: 
    #         https://valheel.fandom.com/wiki/Item:_Mirra_Infused_Pickaxe
    # """
    """
    """
    end
end
