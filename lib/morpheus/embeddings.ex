defmodule Morpheus.Embeddings do
    use Agent

    alias Morpheus.StorageBucket

    @dimensions 768
    @max_embeddings 500
    @vector_space :cosine

    def start_link(_opts) do
        Agent.start_link(&create_index/0, name: __MODULE__)
    end

    def get_closest_matching_pages_function() do
        LangChain.Function.new!(%{
            name: "get_closest_matching_pages",
            description: "Takes a search term, and finds the 3 wiki pages that are most relevant",
            parameters_schema: %{
                type: "object",
                properties: %{
                    search_term: %{
                        type: "string",
                        description: "The terms to search for"
                    }
                },
                required: [:search_term],
            },
            function: &get_closest_matching_pages/2
        })
    end

    def get_closest_matching_pages(%{"search_term" => search_term}, _context) do
        with {:ok, pages} <- get_nearest_neighbors(search_term, k: 2) do
            pages
            |> Map.keys()
            |> Enum.map(&Map.take(&1,[:page_title, :page_url, :page_content]))
            |> Enum.uniq_by(& &1.page_url)
            |> then(fn list -> %{result: list} end)
            |> Jason.encode!()
        end
    end

    defp create_index() do
        {:ok, index} = HNSWLib.Index.new(@vector_space, @dimensions, @max_embeddings)
        index
    end

    def create_embedding(title, content) do
        provider().create_embedding(title, content)
    end

    def create_query_embedding(query) do
        provider().create_query_embedding(query)
    end

    def embed_document(title, content, id) do
        with {:ok, embedding} <- create_embedding(title, content) do
            Agent.update(__MODULE__, fn index ->
                HNSWLib.Index.add_items(index, embedding, ids: [id])
                index
            end)

        end
    end

    def get_nearest_neighbors(query, opts \\ []) do
        with {:ok, query_embedding} <- create_query_embedding(query),
             {:ok, ids, dists} <- Agent.get(__MODULE__, fn index ->
                HNSWLib.Index.knn_query(index, query_embedding, opts)
             end) do
            objects =
                ids
                |> Nx.to_flat_list()
                |> Enum.map(fn id ->
                    StorageBucket.get(id)
                end)

            {:ok, Enum.zip(objects, Nx.to_flat_list(dists)) |> Enum.into(%{})}
        end
    end

    defp provider() do
        Application.get_env(:morpheus, :embeddings_provider, Morpheus.Embeddings.Gemini)
    end
end
